package u02

import org.junit.jupiter.api.Test
import org.junit.jupiter.api.Assertions._
import u02.Optionals.Option.{None, Some, filter, map, map2}

class OptionalTest {

  @Test def testFilter(): Unit ={
    assertEquals(Some(1), filter(Some(1))(_ > 0))
    assertEquals(None(), filter(Some(1))(_ < 0))
  }

  @Test def testMap(): Unit ={
    assertEquals(Some(true), map(Some(1))(_ > 0))
    assertEquals(None(), map(None[Int]())(_ > 0))
    assertEquals(Some(false), map(Some(1))(_ < 0))
  }

  @Test def testMap2(): Unit ={
    assertEquals(Some(2), map2(Some(1), Some(1))(_ + _))
    assertEquals(None(), map2(Some(2), None[Int]())(_ - _))
  }
}
