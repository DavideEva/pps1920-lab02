package u02.lab02

import java.util.function.Predicate

object Exercise3 {

  val parity1: Int => String = x => x % 2 match {
    case 0 => "even"
    case _ => "odd"
  }
  //--------------------------------------------------------------------------

  def parity2(x: Int): String = x % 2 match {
    case 0 => "even"
    case _ => "odd"
  }

  def negPredicate[T](p: Predicate[T]): Predicate[T] = !p.test(_)

  //--------------------------------------------------------------------------
  //Senza l'uso di Predicate

  def neg[T](x: T => Boolean): T => Boolean = !x(_)

}
