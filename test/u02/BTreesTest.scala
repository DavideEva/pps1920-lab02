package u02

import org.junit.jupiter.api.Assertions._
import org.junit.jupiter.api.Test

class BTreesTest {

  import BTrees.Tree._
  val tree = Branch(Branch(Leaf(1),Leaf(2)),Leaf(1))

  @Test def testCount(){
    assertEquals(2, count(tree,1))
    assertEquals(1, count(tree,2))
    assertEquals(0, count(tree,3))
  }

  @Test def testSize(){
    assertEquals(3, size(tree))
  }

  @Test def testFind(){
    assertTrue(find(tree,2))
    assertFalse(find(tree,4))
  }

  @Test def testSizeTraverse(): Unit ={
    assertEquals(3, sizeTraverse(tree))
  }

  @Test def testFindTraverse(): Unit ={
    assertTrue(findTraverse(tree, 1))
    assertFalse(findTraverse(tree, 0))
  }

  @Test def testCountTraverse(): Unit ={
    assertEquals(1, countTraverse(tree,2))
    assertEquals(0, countTraverse(tree,0))
  }
}