package u02.lab02

import org.junit.jupiter.api.Assertions._
import org.junit.jupiter.api.Test
import u02.lab02.Exercise4._;

class Exercise4Test {

  private val x = 1
  private val y = 2
  private val z = 3

  @Test def testP3(): Unit ={
    assertTrue(p3(x)(y)(z))
    assertFalse(p3(x)(z)(y))
    assertTrue(p3(x)(x)(z))
  }

  @Test def testP4(): Unit ={
    assertTrue(p4(x, y, z))
    assertFalse(p4(x, z, y))
    assertTrue(p4(x, x, z))
  }
}
