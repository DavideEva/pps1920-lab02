package u02.lab02

import org.junit.jupiter.api.Test
import org.junit.jupiter.api.Assertions._
import u02.lab02.Exercise7.Shape._

class Exercise7Test {

  private val DELTA: Double = 1e-5
  private val rBase = 10
  private val rHeights = 20
  private val rect: Rectangle = Rectangle(rBase, rHeights)
  private val rCircle = 10
  private val circle: Circle = Circle(rCircle)
  private val lSquare = 10
  private val square: Square = Square(lSquare)

  @Test def testPerimeter(): Unit ={
    assertEquals(2*(rBase+rHeights), perimeter(rect))
    assertEquals(2*rCircle*Math.PI, perimeter(circle), DELTA)
    assertEquals(4*lSquare, perimeter(square))
  }

  @Test def testArea(): Unit ={
    assertEquals((rBase*rHeights), area(rect))
    assertEquals(rCircle*rCircle*Math.PI, area(circle), DELTA)
    assertEquals(lSquare*lSquare, area(square))
  }
}
