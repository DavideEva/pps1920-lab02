package u02.lab02

object Exercise7 {

  sealed trait Shape
  object Shape {
    case class Rectangle(b: Int, h: Int) extends Shape
    case class Square(l: Int) extends Shape
    case class Circle(r: Int) extends Shape

    def perimeter(shape: Shape): Double = shape match {
      case Rectangle(b, h) => 2*(b+h)
      case Square(l) => 4*l
      case Circle(r) => 2*Math.PI*r
    }

    def area(shape: Shape): Double = shape match {
      case Rectangle(b, h) => b*h
      case Square(l) => l*l
      case Circle(r) => Math.PI*r*r
    }
  }
}
