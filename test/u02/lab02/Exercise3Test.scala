package u02.lab02

import java.util.function.Predicate

import org.junit.jupiter.api.Assertions._
import org.junit.jupiter.api.Test
import u02.lab02.Exercise3._;

class Exercise3Test {

  @Test def testParity2(){
    assertEquals("odd", parity2(1));
    assertEquals("even", parity2(2))
  }

  @Test def testNegPredicate(){
    val emptyPredicate: Predicate[String] = _==""
    val notEmptyPredicate: Predicate[String] = negPredicate[String](emptyPredicate)
    assertTrue(notEmptyPredicate.test("foo"))                                    //true
    assertFalse(notEmptyPredicate.test(""))                                       //false
    assertTrue(notEmptyPredicate.test("foo") && !notEmptyPredicate.test(""))     //true
  }

  @Test def testNegGenericPredicate(): Unit ={
    val positive: Predicate[Int] = _>0
    val notPositive: Predicate[Int] = negPredicate(positive)
    assertTrue(notPositive.test(-1))
    assertFalse(notPositive.test(1))
    assertTrue(notPositive.test(-1) && !notPositive.test(1))
  }

  @Test def testNeg(): Unit ={
    val empty: String => Boolean = _==""
    val notEmpty = neg(empty)
    assertTrue(notEmpty("foo"))
    assertFalse(notEmpty(""))
    assertTrue(notEmpty("foo") && !notEmpty(""))
  }

  @Test def testNegGeneric(): Unit ={
    val even: Int => Boolean = _%2 == 0
    val notEven = neg(even)
    assertTrue(notEven(1))
    assertFalse(notEven(0))
    assertTrue(notEven(1) && !notEven(0))
  }
}
