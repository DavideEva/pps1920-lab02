package u02.lab02

import scala.annotation.tailrec

object Exercise6 {

  def fib(n: Int): Int = {
    @tailrec
    def _factFib(n: Int, last: Int, secondToLast: Int): Int = n match {
      case 0 => 0
      case 1 => last
      case _ => _factFib(n-1, last+secondToLast, last)
    }
    _factFib(n, 1, 0)
  }
}
