package u02.lab02

object Exercise5 {

  def compose1(f: Int => Int, g: Int => Int): (Int => Int) = x => f(g(x))
  def compose2[T](f: T => T, g: T => T): (T => T) = x => f(g(x))
  def compose3[T1, T2, T3](f: T2 => T3, g: T1 => T2): (T1 => T3) = x => f(g(x))
}
