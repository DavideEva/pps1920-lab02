package u02.lab02

import org.junit.jupiter.api.Assertions._
import org.junit.jupiter.api.Test
import u02.lab02.Exercise5._;

class Exercise5Test {

  @Test def testCompose1(): Unit ={
    assertEquals(9, compose1(_-1,_*2)(5))
    assertEquals(8, compose1(_*2,_-1)(5))
  }

  @Test def testCompose2(): Unit ={
    assertEquals(9, compose2[Int](_-1,_*2)(5))
    assertEquals(8, compose2[Int](_*2,_-1)(5))
  }

  @Test def testCompose3(): Unit ={
    assertEquals(9.0, compose3[Int, Double, Double](_-1,_*2)(5))
    assertEquals(8.0, compose3[Int, Double, Double](_*2,_-1)(5))
  }
}
