package u02.lab02

import org.junit.jupiter.api.Assertions._
import org.junit.jupiter.api.Test
import Exercise6.fib;

class Exercise6Test {

  @Test def testFib(): Unit ={
    assertEquals(0, fib(0))
    assertEquals(1, fib(1))
    assertEquals(1, fib(2))
    assertEquals(2, fib(3))
    assertEquals(3, fib(4))
    assertEquals(5, fib(5))
    assertEquals(8, fib(6))
    assertEquals(13, fib(7))
  }
}
